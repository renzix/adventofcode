#!/usr/bin/env perl6
# Old answer to part1
#my $freq=0;
#for '../input.txt'.IO.lines -> $line {
#    $freq+=$line;
#}
my $fh = '../input.txt'.IO.lines.eager;

say "Awnser 1: " ~ $fh.sum;
say "Awnser 2: " ~ (loop { $fh.eager }).lazy.flat.produce(&[+]).repeated.first;

