#include <limits.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void) {
  FILE *input;
  input = fopen("../input.txt", "r");
  if (input == NULL) {
    puts("cannot open said file");
    return 1;
  }
  /*
    fseek(input,0L,SEEK_END);

    long bufsize = ftell(input);
    if (bufsize == -1) {
        puts("Cannot find length of file");
        return 2;
    }

    int cur_freq = 0;
    int freq_length = 0, freq_max = 100;
    char *freq_list = malloc(sizeof(char) * (bufsize+1));
    rewind(input);
    fread(freq_list, sizeof(char), bufsize, input);
    fclose(input);

    char temp_storage[20];
    */
  int cur_freq;
  int freq_length, freq_max;
  freq_length = 0;
  freq_max = 100;
  int *freq_list = malloc(sizeof(int) * freq_max);
  char buffer[7] = "";
  while (fscanf(input, "%s", buffer) != EOF) {
    if ((cur_freq = atoi(buffer)) == 0)
      return 1;
    if (freq_length >= freq_max)
      freq_list = realloc(freq_list, (freq_max *= 2) * sizeof(int));
    freq_list[freq_length++] = cur_freq;
  }
  bool found = false;
  int freq_total = 0;
  int found_freq_max, found_freq_length;
  found_freq_length = 0;
  found_freq_max = 200;
  int *found_freq_list = malloc(sizeof(int) * found_freq_max);
  while (found != true) {
    // loop through buffer
    if (freq_list[freq_length] == '\0')
      freq_length = 0;
    freq_total += freq_list[freq_length++];
    // add to found freq
    if (found_freq_length >= found_freq_max)
      found_freq_list =
          realloc(found_freq_list, (found_freq_max += 10) * sizeof(int));
    // check if it is in found_freq_list
    for (int freq = 0; freq < found_freq_length; freq++) {
      if (found_freq_list[freq] == freq_total) {
        printf("Answer 2: %i\n", freq_total);
        found = true;
      }
    }
    found_freq_list[found_freq_length++] = freq_total;
  }
  free(freq_list);
  free(found_freq_list);

  // printf("%c\n",freq_list[2]);
  /*char buffer[10] = ""; // Can make this lower or use malloc but this is fine
  memset(freq_list, INT_MIN, freq_max);
  bool found_reoccuring = false, first_time=true;
  while (!found_reoccuring) {
    while (fscanf(input, "%s", buffer) != EOF) {
      if ((cur_freq = atoi(buffer)) == 0)
        return 1;
      total_freq += cur_freq;
      if (!found_reoccuring) {
        if (freq_length >= freq_max)
          freq_list = realloc(freq_list, (freq_max *= 2) * sizeof(int));

        for (int freq = 0; freq < freq_length; freq++) {
          if (freq_list[freq] == total_freq) {
            printf("Answer 2: %i\n", total_freq);
            found_reoccuring = true;
          }
        }
        freq_list[freq_length++] = total_freq;
      }
    }
    if (first_time) {
      printf("Answer 1: %i\n", total_freq);
      first_time=false;
    }
    rewind(input);
  }*/
}
