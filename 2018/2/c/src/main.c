#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LINE_SIZE 30

bool check_how_many(char *str, __int8_t size, __int8_t check_value);
int strdiff(char* str1,char* str2,char* combination, int length);

int main(void) {
  // Variables
  FILE *input;
  input = fopen("../input.txt", "r");
  if (input == NULL) {
    puts("cannot open said file");
    return 1;
  }
  char buffer[LINE_SIZE];
  int boxes_length = 0, boxes_max = 100;
  char(*boxes)[LINE_SIZE] = malloc(sizeof(char) * LINE_SIZE * boxes_max);
  memset(boxes, '\0', boxes_max);
  int id2 = 0, id3 = 0;
  // First part mainly just finds checksum and adds the matches to a dynamically
  // allocated array
  while (fscanf(input, "%s", buffer) != EOF) {
    if (check_how_many(buffer, LINE_SIZE, 2)) { //@SPEED(renzix): Again i probably could use a actual algo and
      id2++;                                    // also maybe combine this with the 3 for speed reasons
      strcpy(boxes[boxes_length++], buffer);
      if (boxes_length >= boxes_max)
        boxes = realloc(boxes, sizeof(char) * LINE_SIZE * (boxes_max *= 1.6));
    }
    if (check_how_many(buffer, LINE_SIZE, 3)) {
      id3++;
      strcpy(boxes[boxes_length++], buffer);
      if (boxes_length >= boxes_max)
        boxes = realloc(boxes, sizeof(char) * LINE_SIZE * (boxes_max *= 1.6));
    }
  }
  fclose(input);
  printf("Awnser 1: %i\n", id2 * id3);
  // Second part where I find the 2 similar boxes
  bool answer_found = false;
  for (int i = 0; (i < boxes_length)&&(!answer_found); i++) { //@SPEED(renzix): Probably could use a actual agorithm
    for (int j = 0; j < boxes_length; j++) {
        if (i==j)
            continue;
        if (strdiff(boxes[i],boxes[j],buffer,LINE_SIZE)==1){ //@SPEED(renzix): It might be better to just make this
          printf("Awnser 2: %s\n",buffer);                   // 2 seperate functions so that i dont write to a buffer for no reason
          answer_found=true;
        }
    }
  }
  free(boxes);
}

bool check_how_many(char *str, __int8_t size, __int8_t check_value) {
  __int8_t matches = 0;
  for (int i = 0; i < size; i++) {
    for (int j = 0; j < size; j++) {
      if (str[i] == str[j]) {
        matches++;
      }
    }
    if (matches == check_value)
      return true;
    else
      matches = 0;
  }
  return false;
}

// Only compares char* of the same length
int strdiff(char* str1,char* str2,char* combination, int length){
    int result = 0;
    memset(combination,'\0',length);
    for (int i=0;i<length;i++){
        if (str1[i]!=str2[i])
            result++;
        else
            combination[i-result]=str1[i];
    }
    return result;
}
