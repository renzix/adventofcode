#include "map.h"

static void map_realloc(int width);

void map_init(void){
    /// Allocate memory
    map_width=map_height=0;
    map_width_max=INIT_WIDTH;
    map_height_max=INIT_HEIGHT;
    map = (bool**) malloc(INIT_WIDTH*sizeof(bool*));
    for (int i=0;i<INIT_WIDTH;i++){
        map[i] = (bool*) malloc(INIT_HEIGHT*sizeof(bool));
    }
}

void map_mark(int from_left, int from_top ,int width, int height){
    // first realloc if needed
    if(from_left+width>map_width_max)
        map_realloc(map_width_max*1.6*sizeof(bool*));
}

static void map_width_realloc(int width){
    map = realloc(map, width);
}

void map_destroy(void){
    for (int i=0;i<map_width_max;i++)
        free(map[i]);
    free(map);
}
