#ifndef __MAP_H_
#define __MAP_H_

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

#define INIT_HEIGHT 100
#define INIT_WIDTH 100

bool** map;
int map_width;
int map_height;
int map_height_max;
int map_width_max;

void map_init(void);
void map_mark(int from_left, int from_top ,int width, int height);
void map_destroy(void);

#endif // __MAP_H_
