#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#include "map.h"

#define LINE_SIZE 30

void get_data_from_line(char *line, int size, int *line_data);
int parse_for_num(char *line, int start);

int main(void) {
  FILE *input;
  input = fopen("input.txt", "r");
  if (input == NULL)
    return 1;
  char line[LINE_SIZE];
  memset(line, '\0', LINE_SIZE);
  int line_data[5];
  while (fgets(line, LINE_SIZE, input) != NULL) { // For every line
    get_data_from_line(line, LINE_SIZE, line_data);
  }
  fclose(input);
}

void get_data_from_line(char *line, int size, int *line_data) {
  line_data[1] = parse_for_num(line,1);
  char* point;
  int index;
  point = strchr(line,'@');
  index = (int)(point-line+2);
  line_data[2] = parse_for_num(line,index);
  point = strchr(line,',');
  index = (int)(point-line+1);
  line_data[3] = parse_for_num(line,index);
  point = strchr(line,':');
  index = (int)(point-line+2);
  line_data[4] = parse_for_num(line,index);
  point = strchr(line,'x');
  index = (int)(point-line+1);
  line_data[5] = parse_for_num(line,index);
  /* printf("id:%i, from_left:%i, from_top: %i, width: %i, height: %i\n",line_data[1],line_data[2],line_data[3],line_data[4],line_data[5]); */
}

int parse_for_num(char *line, int start) {
  char buffer[LINE_SIZE];
  memset(buffer, 0, LINE_SIZE);
  for (int bi = 0, li = start; isdigit(line[li]); bi++, li++) {
    buffer[bi] = line[li];
  }
  return atoi(buffer);
}
