#!/usr/bin/env python3

import math


def get_fuel_rec(mass: float) -> float:
    total = 0
    while True:
        mass = math.floor(float(mass)/3) - 2
        if (mass <= 0):
            break
        else:
            total += mass
    return total


with open('../input.txt') as input:
    total = 0
    for mass in input:
        total += math.floor(float(mass)/3) - 2

print("Part 1: {}".format(total))

with open('../input.txt') as input:
    total = 0
    for mass in input:
        total += get_fuel_rec(mass)

print("Part 2: {}".format(total))
