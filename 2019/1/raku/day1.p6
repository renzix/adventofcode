#!/usr/bin/env perl6

my @input =  "../input.txt".IO.lines()>>.Num;
say "Part 1: " ~ [+] @input.race.map({ ($_/3).floor - 2 });
say "Part 2: " ~ [+] @input.race.map({-$_ + [+] +$_, (* / 3).floor - 2 ...^ * < 0 });
