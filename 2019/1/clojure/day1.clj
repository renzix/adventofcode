#!/usr/bin/env clojure-1.8

;; Partly stolen from reddit

(defn fuel-cost [x]
  (- (quot x 3) 2))

(defn fuel-cost-rec [mass]
  (loop [fuel-mass (fuel-cost mass) total 0]
    (if (<= fuel-mass 0)
      total
      (recur (fuel-cost fuel-mass) (+ total fuel-mass)))))

(defn day1 []
  (let [input (map #(Integer/parseInt %) (clojure.string/split-lines (slurp "../input.txt")))]
    (println (format "Part 1: %d" (reduce + (map fuel-cost input))))
    (println (format "Part 2: %d" (reduce + (map fuel-cost-rec input))))))

(day1)
