#include <iostream>
#include <fstream>
#include <cmath>

int get_fuel(double mass);
int get_fuel_rec(double mass);

int main() {
    int total = 0;
    std::ifstream infile("../input.txt");
    if (infile.is_open()) {
        std::string line;
        while (std::getline(infile,line)) {
            total += get_fuel(stod(line));
        }
    }
    std::cout << "Part 1: " << total << std::endl;
    total = 0;
    infile.clear();
    infile.seekg(0);
    if (infile.is_open()) {
        std::string line;
        while (std::getline(infile,line)) {
            total += get_fuel_rec(stod(line));
        }
    }
    infile.close();
    std::cout << "Part 2: " << total << std::endl;
    return 0;
}

int get_fuel(double mass) {
    return std::floor(mass/3) - 2;
}

int get_fuel_rec(double mass) {
    int total = -mass;
    do {
        total+=mass;
        mass = std::floor(mass/3) - 2;
    }while (mass>0);
    return total;
}
