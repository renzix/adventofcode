use std::fs;
use std::io::{self,Read};

// REWRITTEN FROM REDDIT NOT MY SOLUTION

fn get_fuel_rec(mut mass: usize) -> usize{
    let mut total_fuel = 0;
    while let Some(m) = (mass/3).checked_sub(2) {
        total_fuel+=m;
        mass=m;
    }
    total_fuel
}

fn main() {
    let content = fs::read_to_string("../input.txt").unwrap();
    let part1 = content.lines()
                       .map(|mass| { (mass.parse::<usize>().unwrap()/3) - 2 })
                       .sum::<usize>();
    let part2 = content.lines()
                       .map(|mass| { get_fuel_rec(mass.parse().unwrap())})
                       .sum::<usize>();
    println!("Part 1: {:?}", part1);
    println!("Part 2: {:?}", part2);
}
