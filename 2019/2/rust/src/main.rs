
fn main() {
    let memory = std::fs::read_to_string("../input.txt").unwrap();
    let memory: Vec<i32> = memory.split(',').map(|data| data.trim().parse::<i32>().unwrap() ).collect();
    println!("Part 1: {}", machine(&memory,12,2));
    for noun in 0..99 {
        for verb in 0..99 {
            if machine(&memory,noun,verb) == 19690720 {
                println!("Part 2: {}", 100*noun+verb);
            }
        }
    }
}

fn machine(memory: &Vec<i32>, noun: i32, verb: i32) -> i32 {
    let mut memory = memory.clone();
    let mut index = 0;
    memory[1] = noun;
    memory[2] = verb;
    loop {
        match memory[index] {
            1 => {
                let left_loc = memory[index+1] as usize;
                let right_loc = memory[index+2] as usize;
                let result_loc = memory[index+3] as usize;
                memory[result_loc] = memory[left_loc] + memory[right_loc];
                index+=4;
            },
            2 => {
                let left_loc = memory[index+1] as usize;
                let right_loc = memory[index+2] as usize;
                let result_loc = memory[index+3] as usize;
                memory[result_loc as usize] = memory[left_loc] * memory[right_loc];
                index+=4;
            },
            99 => break,
            _ => panic!(),
        }
    }
    memory[0]
}
