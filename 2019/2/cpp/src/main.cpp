#include <iostream>
#include <fstream>
#include <string>
#include <vector>

int machine(std::vector<int> memory, int noun, int verb);

enum opcode {
   ADD = 1,
   MUL = 2,
   END = 99
};

int main(int argc, char *argv[]) {
    // Part 1
    std::ifstream infile("../input.txt");
    std::vector<int> memory;
    if (infile.is_open()) {
        std::string line;
        while (std::getline(infile,line,',')) {
            memory.push_back(std::stoi(line));
        }
    }
    infile.close();


    std::cout << "Part 1: " << machine(memory,12,2) << std::endl;

    // Part 2
    for (int noun = 0;noun<100;noun++) {
        for (int verb = 0; verb<100; verb++) {
            if (machine(memory,noun,verb) == 19690720) {
                std::cout << "Part 2: " << 100*noun+verb << std::endl;
                std::exit(EXIT_SUCCESS);
            }
        }
    }

    return EXIT_FAILURE;
}

int machine(std::vector<int> memory,int noun, int verb) {
    int index=0;
    memory[1] = noun;
    memory[2] = verb;
    for (;;) {
        switch (memory[index]) {
            case ADD:
                memory[memory[index+3]] = memory[memory[index+1]] + memory[memory[index+2]];
                index+=4;
                break;
            case MUL:
                memory[memory[index+3]] = memory[memory[index+1]] * memory[memory[index+2]];
                index+=4;
                break;
            case END:
                return memory[0];
            default:
                throw 1; // Not valid opcode
        }
    }
}
