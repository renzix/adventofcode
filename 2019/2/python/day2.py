#!/usr/bin/env python3

from copy import deepcopy

memory = []

with open("../input.txt") as input:
    memory = input.read().split(",");
    memory = [int(i) for i in memory]


def machine(memory, noun, verb):
    index = 0
    memory[1] = noun
    memory[2] = verb
    while index<len(memory):
        if memory[index] == 1:
            memory[memory[index+3]] = memory[memory[index+1]] + memory[memory[index+2]]
            index+=4
        elif memory[index] == 2:
            memory[memory[index+3]] = memory[memory[index+1]] * memory[memory[index+2]]
            index+=4
        elif memory[index] == 99:
            break
        else:
            raise Exception("Cannot find opcode")
    return memory[0]

print("Part 1: {}".format(machine(deepcopy(memory), 12, 2)))
for noun in range(100):
    for verb in range(100):
        if (machine(deepcopy(memory), noun, verb) == 19690720):
            print("Part 2: {}".format(100*noun+verb))
            break
    else:
        continue
    break
