#!/usr/bin/env clojure-1.8

;; Inspiration taken from reddit but all done by myself

(defn parse [filename]
  (as-> filename f
    (slurp f)
    (clojure.string/split f #",")
    (map read-string f)
    (vec f)))

(def operations {
                 1 (fn [data pointer]
                     (let [first-value (get data (get data (+ pointer 1)))
                           second-value (get data (get data (+ pointer 2)))
                           output-location (get data (+ pointer 3))]
                       (assoc data output-location (+ first-value second-value))))

                 2 (fn [data pointer]
                     (let [first-value (get data (get data (+ pointer 1)))
                           second-value (get data (get data (+ pointer 2)))
                           output-location (get data (+ pointer 3))]
                       (assoc data output-location (* first-value second-value))))})

(defn run [program-data instruction-pointer]
  (loop [data program-data
         pointer instruction-pointer]
    (let [opcode (get data pointer)]
      (if (= opcode 99)
        data
        (recur (apply (get operations opcode) [data pointer])
               (+ pointer 4))))))

(defn part1 [filename]
  (-> filename
      parse
      (assoc 1 12)
      (assoc 2 02)
      (run 0)
      (get 0)))

(def all-combos (for [noun (range 100) verb (range 100)]
                  (vector noun verb)))

(defn part2 [filename]
  (-> filename
      parse
      (assoc 1 noun)
      (assoc 2 verb)
      (run 0)
      (get 0)))

(defn day2 []
  (println (format "Part 1: %d" (part1 "../input.txt")))
  (println (format "Part 2: %d" (part2 "../input.txt"))))

(day2)
