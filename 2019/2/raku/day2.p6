#!/usr/bin/env perl6

my @orig = "../input.txt".IO.comb(/\d+/);
my @noun = 0..99;
my @verb = 0..99;
my @input = @orig;

my $index=0;
for @verb -> $verb {
    for @noun -> $noun {
        @input = @orig;
        @input[1] = $noun;
        @input[2] = $verb;
        $index=0;
        while $index < @input.elems {
            given @input[$index] {
                when 1 {
                    @input[@input[$index+3]] = @input[@input[$index+1]] + @input[@input[$index+2]];
                    $index+=4;
                    next;
                }
                when 2 {
                    @input[@input[$index+3]] = @input[@input[$index+1]] * @input[@input[$index+2]];
                    $index+=4;
                    next;
                }
                when 99 {
                    last;
                }
            }
            $index++;
        }
        say "Part 2: "~ 100 * $noun + $verb if @input[0] == 19690720;
    }
}
