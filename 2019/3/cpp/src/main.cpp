#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <vector>
#include <algorithm>
#include <cstdlib>

struct directions {
    char direct;
    int length;
};

struct coord {
    int x;
    int y;
};

bool compare_coords(coord const& lhs, coord const& rhs);
bool equal_coords(coord const& lhs, coord const& rhs);
std::vector<coord> get_path(std::vector<directions> wire, int x, int y);
std::vector<std::string> split(const std::string& s, char delimiter);

int main(int argc, char *argv[]) {
    std::ifstream infile("../input.txt");
    std::vector<directions> wire1;
    std::vector<directions> wire2;
    if (infile.is_open()) {
        std::string temp_string;
        // Not worth iterating over if only doing twice and copy/paste is a thing
        if (std::getline(infile,temp_string)) {
            for (const std::string &directions : split(temp_string,',')){
                struct directions single_directions;
                single_directions.direct = directions.front();
                single_directions.length = std::stoi(directions.substr(1));
                wire1.push_back(single_directions);
            }
        }
        temp_string.clear();
        if (std::getline(infile,temp_string)) {
            for (const std::string &directions : split(temp_string,',')){
                struct directions single_directions;
                single_directions.direct = directions.front();
                single_directions.length = std::stoi(directions.substr(1));
                wire2.push_back(single_directions);
            }
        }
    }

    std::vector<coord> wire1_path = get_path(wire1,0,0);
    std::vector<coord> wire2_path = get_path(wire2,0,0);
    std::sort(wire1_path.begin(),wire1_path.end(),compare_coords);
    std::sort(wire2_path.begin(),wire2_path.end(),compare_coords);

    std::vector<coord> intersections(wire1_path.size()+wire2_path.size());

    std::set_intersection(wire1_path.begin(), wire1_path.end(),
                          wire2_path.begin(), wire2_path.end(),
                          intersections.begin(),
                          compare_coords);
    auto last = std::unique(intersections.begin(),intersections.end(), equal_coords);
    intersections.erase(last,intersections.end());
    std::vector<int> manhatten_distance;
    int min = 999999;
    for (const coord &intersec : intersections){
        if (intersec.x==0&&intersec.y) {
            continue;
        }
        if (abs(intersec.x)+abs(intersec.y)<min) {
            min = abs(intersec.x)+abs(intersec.y);
        }
    }
    std::cout << min << std::endl;;
    return 0;
}

bool compare_coords(coord const& lhs, coord const& rhs) {
    return (abs(lhs.x)+abs(lhs.y)) < (abs(rhs.x)+abs(rhs.y));
}

bool equal_coords(coord const& lhs, coord const& rhs) {
    return (abs(lhs.x)+abs(lhs.y)) == (abs(rhs.x)+abs(rhs.y));
}

std::vector<coord> get_path(std::vector<directions> wire, int x, int y) {
    std::vector<coord> wire_path;
    for (const directions &direct : wire) {
        switch(direct.direct) {
            case 'U':
                for (int inc_y=y;direct.length+y>=inc_y;inc_y++){
                    struct coord new_coord;
                    new_coord.x = x;
                    new_coord.y = inc_y;
                    wire_path.push_back(new_coord);
                }
                y+=direct.length;
                break;
            case 'D':
                for (int dec_y=y;direct.length-y<=dec_y;dec_y--){
                    struct coord new_coord;
                    new_coord.x = x;
                    new_coord.y = dec_y;
                    wire_path.push_back(new_coord);
                }
                y-=direct.length;
                break;
            case 'L':
                for (int dec_x=x;direct.length-x<=dec_x;dec_x--){
                    struct coord new_coord;
                    new_coord.x = dec_x;
                    new_coord.y = y;
                    wire_path.push_back(new_coord);
                }
                x-=direct.length;
                break;
            case 'R':
                for (int inc_x=x;direct.length+x>=inc_x;inc_x++){
                    struct coord new_coord;
                    new_coord.x = inc_x;
                    new_coord.y = y;
                    wire_path.push_back(new_coord);
                }
                x+=direct.length;
                break;
            default:
                throw;
        }
    }
    return wire_path;
}

std::vector<std::string> split(const std::string& s, char delimiter) {
    std::vector<std::string> tokens;
    std::string token;
    std::istringstream token_stream(s);
    while (std::getline(token_stream, token, delimiter)) {
        tokens.push_back(token);
    }
    return tokens;
}
