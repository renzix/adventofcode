#!/usr/bin/env perl6

my @direction1 = "../input.txt".IO.split(/\n/)[0].comb(/\D/);
my @distance1 = "../input.txt".IO.split(/\n/)[0].comb(/\d+/);
my @direction2 = "../input.txt".IO.split(/\n/)[1].comb(/\D/);
my @distance2 = "../input.txt".IO.split(/\n/)[1].comb(/\d+/);
my @what-it-did1 = zip(@direction1,@distance1);
my @what-it-did2 = zip(@direction2,@distance2);


my @places1;
my @places2;

sub get-route(@places, @what-it-did) {
    my $x = 0;
    my $y = 0;
    for @what-it-did -> ($direction,$distance) {
        given $direction {
            when /U/ {
                for $y..($distance+$y) -> $i {
                    @places.push(($x.clone(),$i.clone()));
                }
                $y+=$distance
            }
            when /D/ {
                for ($y-$distance)..$y -> $i {
                    @places.push(($x.clone(),$i.clone()));
                }
                $y-=$distance
            }
            when /L/ {
                for ($x-$distance)..$x -> $i {
                    @places.push(($i.clone(),$y.clone()));
                }
                $x-=$distance
            }
            when /R/ {
                for $x..($x+$distance) -> $i {
                    @places.push(($i.clone(),$y.clone()));
                }
                $x+=$distance
            }
        }
    }
}

get-route(@places1,@what-it-did1);
get-route(@places2,@what-it-did2);

#say (@places1 (&) @places2).map({ $_[0].abs + $_[1].abs }).min; // Should work but doesnt
@places1 = @places1.unique;
say @places1.push(@places2.unique).repeated(:with(&[eqv])).map({ $_[0].abs + $_[1].abs }).min;
